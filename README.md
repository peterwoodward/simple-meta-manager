# Simple Meta Manager #
Version 0.5

## Features ##
* Adds option for custom page titles, og image and descriptions for each page
* Adds option for Global Page title and description as well as keywords and og image
* Generates a Google sitemap every time a Post or Page is updated
* Generates proper meta tags for Facebook and Twitter
* Adds Google Analytics Tracking for Specified domains (multi-site friendly)
* Removes WordPress Emoji Code

Include in header.php using the following:

```
#!php

if(function_exists('meta_the_header')){meta_the_header();}
```


Include Google Analytics in footer.php using the following:

```
#!php

if(function_exists('meta_the_analytics_code')){meta_the_analytics_code();}
```
