<?php
function seo_create_sitemap() {
  $postsForSitemap = get_posts(array(
    "numberposts" => -1, // -1 specifies ALL posts
    "orderby" => "modified",
    "post_type"  => array("post","page"), // add any CPTs here as well
    "order"    => "DESC"
  ));

  $sitemap = '<?xml version="1.0" encoding="UTF-8"?>';
  $sitemap .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' ;

  // if front page = posts page
  if ( is_front_page() && is_home() ) {

    $sitemap .= '<url>'.
      '<loc>'. get_bloginfo('wpurl') .'</loc>'.
      '<priority>1.00</priority>'.
    '</url>';
  }

  // Begin our Loop
  foreach($postsForSitemap as $post) {
    setup_postdata($post);

    $postdate = explode(" ", $post->post_modified);

    $sitemap .= "<url>".
      "<loc>". get_permalink($post->ID) ."</loc>".
      "<lastmod>". $postdate[0] ."</lastmod>".
      "<changefreq>monthly</changefreq>".
    "</url>";
  }

  $sitemap .= "</urlset>";

  // Specify the path for our sitemap
  $fp = fopen(ABSPATH . "sitemap.xml", "w");
  fwrite($fp, $sitemap);
  fclose($fp);
}
// When a post or page is published
add_action("publish_post", "seo_create_sitemap");
add_action("publish_page", "seo_create_sitemap");

// When a post or page is trashed
add_action("trash_post",   "seo_create_sitemap");
add_action("trash_page",   "seo_create_sitemap");

// When a post or page is restored
add_action("untrash_post", "seo_create_sitemap");
add_action("untrash_page", "seo_create_sitemap");
