<?php
/*
Plugin Name: Simple Meta Manager
Description: Includes for SEO and Social Media meta tags. Requires ACF.
Version:     0.5
Author:      Peter Woodward
*/
?>
<?php
if( function_exists('acf_add_options_page') ) {
acf_add_options_page('Meta Manager');
}

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_meta_man',
	'title' => 'Meta Manager',
	'fields' => array (
		array (
			'key' => 'field_55831dfa9c251',
			'label' => 'Global SEO Page Title',
			'name' => 'global_seo_page_title',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_55831e239c252',
			'label' => 'Global Meta Page Description',
			'name' => 'global_meta_page_description',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_55831e319c253',
			'label' => 'Global Meta Keywords',
			'name' => 'global_meta_keywords',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_55895fb3ab4dc',
			'label' => 'Global Featured Image',
			'name' => 'global_featured_image',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'thumbnail',
			'library' => 'all',
			'min_width' => 400,
			'min_height' => 400,
			'min_size' => '',
			'max_width' => 1500,
			'max_height' => 1500,
			'max_size' => '',
			'mime_types' => '',
		),
		array (
			'key' => 'field_560ac646963fc',
			'label' => 'Google Analytics Code',
			'name' => 'analytics_code',
			'type' => 'repeater',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'min' => '',
			'max' => '',
			'layout' => 'table',
			'button_label' => 'Add Code',
			'sub_fields' => array (
				array (
					'key' => 'field_560ac65d963fd',
					'label' => 'Code',
					'name' => 'code',
					'type' => 'text',
					'instructions' => '',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'readonly' => 0,
					'disabled' => 0,
				),
				array (
					'key' => 'field_560ac665963fe',
					'label' => 'Domain',
					'name' => 'domain',
					'type' => 'text',
					'instructions' => 'This will only include the code if the domain matches this field ie: google.ca',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'readonly' => 0,
					'disabled' => 0,
				),
			),
		)
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options-meta-manager',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
));

acf_add_local_field_group(array (
	'key' => 'group_56e808cd2b586',
	'title' => 'Meta Page SEO',
	'fields' => array (
		array (
			'key' => 'field_56e808e6c9c10',
			'label' => 'Meta Page Title',
			'name' => 'seo_page_title',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_56e808d7c9c0f',
			'label' => 'Meta Page Description',
			'name' => 'meta_page_description',
			'type' => 'textarea',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'maxlength' => '',
			'rows' => '',
			'new_lines' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_56e808f8c9c11',
			'label' => 'Meta OG Image',
			'name' => 'featured_image',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'thumbnail',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'post',
			),
		),
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'page',
			),
		),
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'locations',
			),
		),
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'vacations',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'side',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));


endif;


function seo_create_sitemap() {
  $postsForSitemap = get_posts(array(
    "numberposts" => -1, // -1 specifies ALL posts
    "orderby" => "modified",
    "post_type"  => array("post","page"), // add any CPTs here as well
    "order"    => "DESC"
  ));

  $sitemap = '<?xml version="1.0" encoding="UTF-8"?>';
  $sitemap .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' ;

  // if front page = posts page
  if ( is_front_page() && is_home() ) {

    $sitemap .= '<url>'.
      '<loc>'. get_bloginfo('wpurl') .'</loc>'.
      '<priority>1.00</priority>'.
    '</url>';
  }

  // Begin our Loop
  foreach($postsForSitemap as $post) {
    setup_postdata($post);

    $postdate = explode(" ", $post->post_modified);

    $sitemap .= "<url>".
      "<loc>". get_permalink($post->ID) ."</loc>".
      "<lastmod>". $postdate[0] ."</lastmod>".
      "<changefreq>monthly</changefreq>".
    "</url>";
  }

  $sitemap .= "</urlset>";

  // Specify the path for our sitemap
  $fp = fopen(ABSPATH . "sitemap.xml", "w");
  fwrite($fp, $sitemap);
  fclose($fp);
}
// When a post or page is published
add_action("publish_post", "seo_create_sitemap");
add_action("publish_page", "seo_create_sitemap");

// When a post or page is trashed
add_action("trash_post",   "seo_create_sitemap");
add_action("trash_page",   "seo_create_sitemap");

// When a post or page is restored
add_action("untrash_post", "seo_create_sitemap");
add_action("untrash_page", "seo_create_sitemap");

remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

function meta_get_header(){
	global $post;
	$blog_post_types=array('post', 'blog'); $article_post_types=array();
	if(get_field('meta_page_description', $post->ID) ){  $desc=get_field('meta_page_description', $post->ID);  } else {  $desc=get_field('global_meta_page_description', 'option');  }
	if(get_field('seo_page_title', $post->ID) ){ $title=$post->post_title.' | '.get_field('global_seo_page_title', 'option').' | '.get_field('seo_page_title', $post->ID); } elseif(is_tax()){$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );  $title=$term->name.' | '.get_field('global_seo_page_title', 'option');} elseif(is_archive()){$title=post_type_archive_title('', false).' | '.get_field('global_seo_page_title', 'option');}elseif(is_404()){$title='404 Not Found | '.get_field('global_seo_page_title', 'option');} else {  $title=$post->post_title.' | '.get_field('global_seo_page_title', 'option'); }
	if(get_field('featured_image', $post->ID) ){  $img=get_field('featured_image', $post->ID);  } else {   $img=get_field('global_featured_image', 'option');  }
	if(in_array($post->post_type, $blog_post_types)){$type= 'blog';} elseif(in_array($post->post_type, $article_post_types)){$type= 'article';}else{$type= 'website';}


	$string.= '<meta charset="'.get_bloginfo( 'charset' ).'">
	<meta name="author" content="'.get_bloginfo().'">
	<meta name="description" content="'.$desc.'">
	<meta name="keywords" content="'.get_field('global_meta_keywords', 'option').'">
	<meta name="viewport" content="initial-scale=1.0, width=device-width, maximum-scale=1, user-scalable=no">
	<meta name="format-detection" content="telephone=no">
	<title>'.$title.'</title>';
	/* Open Graph Tags */
	$string.=  '<meta property="og:title" content="'.$title.'">';
	if($img){$string.=  '<meta property="og:image" content="'.$img['sizes']['og_image'].'">';}
	$string.= '<meta property="og:description" content="'.$desc.'">
	<meta property="og:url" content="'.get_permalink($post->ID).'">
	<meta property="og:type" content="'.$type.'">';

	/* Twitter Tags */
	$string.= '<meta name=”twitter:card” content="summary_large_image">
	<meta name="twitter:title" content="'.$title.'">
	';
	if($img){$string.=  '<meta name="twitter:image" content="'.$img['sizes']['og_image'].'">';}
	$string.=  '<meta name="twitter:description" content="'.$desc.'">';
	return $string;
}

function meta_the_header(){
	echo meta_get_header();
}
function meta_get_analytics_code(){
	$return =false;
	if( have_rows('analytics_code', 'options') ):
		while ( have_rows('analytics_code', 'options') ) : the_row();
			if(get_sub_field('domain')==$_SERVER['HTTP_HOST']){
				$return="<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', '".get_sub_field('code')."', 'auto');
		  ga('send', 'pageview');

		</script>";
			}
		endwhile;
		endif;
		return $return;

}
function meta_the_analytics_code(){
		if(meta_get_analytics_code()){
			echo meta_get_analytics_code();
		}
}
